package com.gitlab.davinkevin.appenginejava11.search

import org.slf4j.LoggerFactory
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import org.springframework.web.reactive.function.server.ServerResponse.ok
import org.springframework.web.reactive.function.server.router
import reactor.core.publisher.Mono

@SpringBootApplication
class SearchApplication

fun main(args: Array<String>) {
    runApplication<SearchApplication>(*args)
}

@Configuration
class AppConfiguration {
    @Bean
    fun routes(search: SearchHandler) = router {
        GET("/", search::serve)
    }
}

@Component
class SearchHandler(prop: SearchProperties) {

    private val log = LoggerFactory.getLogger(SearchHandler::class.java)

    val version = prop.version
    val event = prop.event

    fun serve(serverRequest: ServerRequest): Mono<ServerResponse> {

        log.info("Request at search level")

        return ok()
                .syncBody(DemoMessage(event, "search ($version)"))
    }
}

data class DemoMessage(val hello: String, val from: String)

