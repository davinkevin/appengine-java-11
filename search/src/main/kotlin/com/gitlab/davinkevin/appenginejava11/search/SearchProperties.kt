package com.gitlab.davinkevin.appenginejava11.search

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component

/**
 * Created by kevin on 2019-01-13
 */
@Component
@ConfigurationProperties("search")
class SearchProperties {
    lateinit var version: String
    lateinit var event: String
}
